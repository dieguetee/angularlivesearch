var search = angular.module('livesearch',['ui.router', 'ngAnimate' ,'ncy-angular-breadcrumb']);

search.config(function($stateProvider, $urlRouterProvider,$breadcrumbProvider) {
	$breadcrumbProvider.setOptions({
      prefixStateName: 'search'
    });
 

  $urlRouterProvider.otherwise("/");
  $stateProvider
    .state('search', {
      url: "/",
       templateUrl: "templates/search.html",
       controller: "SearchCtrl",
        ncyBreadcrumb: {
           label: 'Search'
         }
    })
    .state('user', {
      url: "/user/:login",
       templateUrl: "templates/userdetail.html",
       controller: "UserDetailCtrl",
       ncyBreadcrumb: {
           label: 'User {{userId}}'
         }
    });
 });

search.controller('SearchCtrl', function($rootScope,$scope,$http,Users){

		$scope.$watch('username', function (value) {
			if (typeof value != 'undefined' && value.length > 3) {
				Users.uservalue(value);
			} else {
				$rootScope.users = null;
			}
		});

		$scope.del = function (event,input) {
		    if (event.keyCode === 8) {
		    	var value = $(event.target).val();
		       if (typeof value != 'undefined' && value.length > 4) {
				Users.uservalue(value);
			} else {
				$rootScope.users = null;
			}
		    }
		};
		
});

search.controller('UserDetailCtrl', function($rootScope,$scope,$http,Users,$stateParams){
		$scope.userId = $stateParams.login;	
		Users.getRepos($scope.userId).then(function(d) {
		    $scope.repositories = d.data;
		    //console.log($scope.repositories);
		});

});

search.factory('Users', function($http,$rootScope) {
 $rootScope.CLIENT_ID = "5850ccc0d49b7561eab9";
 $rootScope.CLIENT_SECRET = "07f587233d9c0e80e5197e6b481546d52e478780";
  var Users = {
    getUsers: function(value) {
      var promise = $http.get('https://api.github.com/search/users?q='+value+'&client_id='+$rootScope.CLIENT_ID+'&client_secret='+$rootScope.CLIENT_SECRET).then(function (response) {
        //console.log(response);
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    getFollowers: function(value) {
      var promise = $http.get(value+'?client_id='+$rootScope.CLIENT_ID+'&client_secret='+$rootScope.CLIENT_SECRET).then(function (response) {
        //console.log(response);
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    getRepos: function (value) {
    	var promise = $http.get('https://api.github.com/users/'+value+'/repos?client_id='+$rootScope.CLIENT_ID+'&client_secret='+$rootScope.CLIENT_SECRET).then(function (response) {
    	  //console.log(response);
    	  return response;
    	});
    	// Return the promise to the controller
    	return promise;
    },
    getCommits: function (user,repo) {
    	var promise = $http.get('https://api.github.com/repos/'+user+'/'+repo+'/commits?client_id='+$rootScope.CLIENT_ID+'&client_secret='+$rootScope.CLIENT_SECRET).then(function (response) {
    	  //console.log(response);
    	  return response.data;
    	});
    	// Return the promise to the controller
    	return promise;
    },
    searchUser: function(users, search){
    	if(!search){
			return users;
		}
		var result = [];
		search = search.toLowerCase();
		angular.forEach(users, function(user){
			if(user.login.toLowerCase().indexOf(search) !== -1){
			result.push(user);
		}
		});
		return result;
    },
    uservalue: function(value){
    	if (value.length == 4) {
    		Users.getUsers(value).then(function(d) {
    		    $rootScope.users = d.items;
    		});
    	} else {
    		var searchuser = Users.searchUser($rootScope.users, value);
    		if (!searchuser.length) {
    			Users.getUsers(value).then(function(d) {
    			  $rootScope.users = d.items;
    			});
    		} else {
    			  $rootScope.users = searchuser;
    		}
    	}
    }
  };
  return Users;
});

search.directive('followers', function(){
	return {
		scope: {
			url : '@'
		}, 
		controller: function($scope,Users) {
			Users.getFollowers($scope.url).then(function(d) {
			   $scope.followers = d;
			});
		},
		restrict: 'E',
		template: '<ul><li ng-repeat="follower in followers | limitTo:3"><a href="#/user/{{follower.login}}">{{follower.login}}</a></li></ul>',
		link: function($scope, iElm, iAttrs, controller) {
			
		}
	};
});

search.directive('commits', function(){
	return {
		scope: {
			user : '@',
			repo: '@'
		}, 
		controller: function($scope,Users) {
			Users.getCommits($scope.user,$scope.repo).then(function(d) {
			   $scope.commits = d;
			   //console.log(d);
			});
		},
		restrict: 'E',
		template: '<ul><li ng-repeat="commit in commits | limitTo:3">{{commit.sha}} | {{commit.commit.committer.name}} | <a href="#/user/{{commit.committer.login}}">{{commit.committer.login}}</a></li></ul>',
		link: function($scope, iElm, iAttrs, controller) {
			
		}
	};
});

search.filter('repofilter', function(){
	return function(repo, reponame){
		if(!reponame){
			return repo;
		}
		var result = [];
		reponame = reponame.toLowerCase();
		angular.forEach(repo, function(item){
			if(item.name.toLowerCase().indexOf(reponame) !== -1){
			result.push(item);
		}
		});
		return result;
	};
});