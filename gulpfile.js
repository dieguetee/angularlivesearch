var gulp = require('gulp');
var webserver = require('gulp-webserver');
// define tasks here
gulp.task('default', function(){
  // run tasks here
  // set up watch handlers here
});



 
gulp.task('serve', function() {
  gulp.src('app')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: true,
      fallback: 'index.html',
      port: 3000
    }));
});